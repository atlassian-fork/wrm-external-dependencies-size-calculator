import { IEntrypointWebresourceDependenciesDescriptor, WebresourceKey } from './chunk-analyzer';
import { IWebresourceSubDependencyResult } from './chunk-analyzer';
import { deduplicate } from './utils';

export interface IWebresourcesSize {
    css: number;
    js: number;
    total: number;
}

export type WebresourceSizeDictionary = Map<WebresourceKey, IWebresourcesSize>;

const ensureAndGetDictionaryEntryForWebresource = (
    dictionary: WebresourceSizeDictionary,
    webresourceKey: WebresourceKey
): IWebresourcesSize => {
    if (!dictionary.has(webresourceKey)) {
        dictionary.set(webresourceKey, {
            css: 0,
            js: 0,
            total: 0,
        });
    }

    return dictionary.get(webresourceKey) as IWebresourcesSize;
};

const fillDirectoryPerChunkGroup = (
    chunkGroup: IWebresourceSubDependencyResult,
    sizeDictionary: WebresourceSizeDictionary,
    sawCssWebresource: Set<string>,
    sawJsWebresource: Set<string>
) => {
    for (const cssGroup of chunkGroup.css.grouped) {
        if (!sawCssWebresource.has(cssGroup.webresourceKey)) {
            const dictionaryEntry = ensureAndGetDictionaryEntryForWebresource(sizeDictionary, cssGroup.webresourceKey);
            dictionaryEntry.css = cssGroup.size;
            dictionaryEntry.total += cssGroup.size;
            sawCssWebresource.add(cssGroup.webresourceKey);
        }
    }

    for (const jsGroup of chunkGroup.js.grouped) {
        if (!sawJsWebresource.has(jsGroup.webresourceKey)) {
            const dictionaryEntry = ensureAndGetDictionaryEntryForWebresource(sizeDictionary, jsGroup.webresourceKey);
            dictionaryEntry.js = jsGroup.size;
            dictionaryEntry.total += jsGroup.size;
            sawJsWebresource.add(jsGroup.webresourceKey);
        }
    }
};

const fillDirectDependencyDictionary = (
    chunkGroup: IWebresourceSubDependencyResult,
    directSizeDictionary: WebresourceSizeDictionary,
    webresourceSizeDictionary: WebresourceSizeDictionary
) => {
    if (!directSizeDictionary.has(chunkGroup.webresourceKey)) {
        const dictionaryEntry = ensureAndGetDictionaryEntryForWebresource(
            directSizeDictionary,
            chunkGroup.webresourceKey
        );
        const subDependencies = deduplicate([...chunkGroup.css.resources, ...chunkGroup.js.resources]);
        for (const subDep of subDependencies) {
            const size = webresourceSizeDictionary.get(subDep);
            if (!size) {
                throw new Error(`Size for dependency: "${subDep}" does not exist.`);
            }
            dictionaryEntry.css += size.css;
            dictionaryEntry.js += size.js;
            dictionaryEntry.total += size.total;
        }
    }
};

export interface ISizeDictionary {
    combined: WebresourceSizeDictionary;
    atomic: WebresourceSizeDictionary;
}

export const generateSizeDictionary = (
    webresourceEntries: Array<IEntrypointWebresourceDependenciesDescriptor>
): ISizeDictionary => {
    const webresourceSizeDictionary: WebresourceSizeDictionary = new Map();
    const directWebresourceSizeDictionary: WebresourceSizeDictionary = new Map();

    const sawCssWebresource = new Set<string>();
    const sawJsWebresource = new Set<string>();

    for (const entry of webresourceEntries) {
        for (const webresourceEntry of entry.result) {
            fillDirectoryPerChunkGroup(
                webresourceEntry,
                webresourceSizeDictionary,
                sawCssWebresource,
                sawJsWebresource
            );
            fillDirectDependencyDictionary(
                webresourceEntry,
                directWebresourceSizeDictionary,
                webresourceSizeDictionary
            );
            for (const asyncChunkGroup of entry.async) {
                for (const asyncWebresourceEntry of asyncChunkGroup.result) {
                    fillDirectoryPerChunkGroup(
                        asyncWebresourceEntry,
                        webresourceSizeDictionary,
                        sawCssWebresource,
                        sawJsWebresource
                    );
                    fillDirectDependencyDictionary(
                        asyncWebresourceEntry,
                        directWebresourceSizeDictionary,
                        webresourceSizeDictionary
                    );
                }
            }
        }
    }

    return {
        combined: directWebresourceSizeDictionary,
        atomic: webresourceSizeDictionary,
    };
};
