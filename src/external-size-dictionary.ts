import { IEntrypointDescriptor, IChunkGroupDescriptor, WebresourceKey, ChunkName } from './chunk-analyzer';
import { ISizeDictionary, IWebresourcesSize, WebresourceSizeDictionary } from './size-dictionary';
import { deduplicate } from './utils';
import { IChunkgroupResourceDependencies, EntryDependencyDescriptor } from './dependency-usage-generator';
import {
    DependencyDescriptor,
    IEntryResourceDependencies,
    IWebresourceDependencies,
} from './dependency-usage-generator';

const getAggregatedSizeForWebresources = (
    webresources: Array<WebresourceKey>,
    sizeDictionary: WebresourceSizeDictionary
): IWebresourcesSize => {
    return webresources.reduce(
        (acc: IWebresourcesSize, dependency) => {
            const size: IWebresourcesSize | undefined = sizeDictionary.get(dependency);
            if (size === undefined) {
                throw new Error(`Could not find sizes for dependency: "${dependency}"`);
            }
            acc.css += size.css;
            acc.js += size.js;
            acc.total += size.total;
            return acc;
        },
        { css: 0, js: 0, total: 0 }
    );
};

const getFlattenedUniqueWebresourceDependencies = (dependenciesPerWebresource: Array<IWebresourceDependencies>) => {
    const dependenciesOfDependency = dependenciesPerWebresource.map(dependency => dependency.dependencies);
    return deduplicate(([] as Array<string>).concat(...dependenciesOfDependency));
};

const getAllFlattenedUniqueWebresourceDependenciesFromChunkgroupResource = (
    chunkgroupResource: IChunkgroupResourceDependencies
) => {
    const direct = chunkgroupResource.result.directDependenciesPerWebresource.map(
        dependency => dependency.dependencies
    );
    const indirect = chunkgroupResource.result.indirectDependenciesPerWebresource.map(
        dependency => dependency.dependencies
    );
    const unique = chunkgroupResource.result.uniqDependenciesPerWebresource.map(dependency => dependency.dependencies);
    return deduplicate(([] as Array<string>).concat(...direct, ...indirect, ...unique));
};

const externalSizeForChunkGroup = (chunkGroup: IChunkgroupResourceDependencies, sizeDictionary: ISizeDictionary) => {
    const dependencies = getAllFlattenedUniqueWebresourceDependenciesFromChunkgroupResource(chunkGroup);
    return getAggregatedSizeForWebresources(dependencies, sizeDictionary.atomic);
};

const inheritedSizeFromEntrypoint = (
    entryChunk: IChunkgroupResourceDependencies,
    childChunk: IChunkgroupResourceDependencies,
    sizeDictionary: ISizeDictionary
) => {
    const entryDependencies = getAllFlattenedUniqueWebresourceDependenciesFromChunkgroupResource(entryChunk);
    const childDependencies = getAllFlattenedUniqueWebresourceDependenciesFromChunkgroupResource(childChunk);
    const childDependenciesAlreadyLoadedInEntry = childDependencies.filter(childDep =>
        entryDependencies.includes(childDep)
    );

    return getAggregatedSizeForWebresources(childDependenciesAlreadyLoadedInEntry, sizeDictionary.atomic);
};

const directDependenciesSize = (
    chunkGroup: IChunkgroupResourceDependencies,
    sizeDictionary: ISizeDictionary
) => {
    return getAggregatedSizeForWebresources(
        getFlattenedUniqueWebresourceDependencies(chunkGroup.result.directDependenciesPerWebresource),
        sizeDictionary.atomic
    );
};

const indirectDependenciesSize = (
    chunkGroup: IChunkgroupResourceDependencies,
    sizeDictionary: ISizeDictionary
) => {
    return getAggregatedSizeForWebresources(
        getFlattenedUniqueWebresourceDependencies(chunkGroup.result.indirectDependenciesPerWebresource),
        sizeDictionary.atomic
    );
};

const uniqueDependenciesSize = (
    chunkGroup: IChunkgroupResourceDependencies,
    sizeDictionary: ISizeDictionary
) => {
    return getAggregatedSizeForWebresources(
        getFlattenedUniqueWebresourceDependencies(chunkGroup.result.uniqDependenciesPerWebresource),
        sizeDictionary.atomic
    );
};

const getDependencyDescriptorForChunkName = (
    descriptor: DependencyDescriptor,
    name: string
): IChunkgroupResourceDependencies => {
    const entryDependencyDescriptor = descriptor.get(name);
    if (entryDependencyDescriptor === undefined) {
        throw new Error(`Could not find dependency descriptor for chunk: "${name}"`);
    }

    return entryDependencyDescriptor;
};

interface IBaseChunkDictionary {
    name: ChunkName;
    externalSize: IWebresourcesSize;
    directSize: IWebresourcesSize;
    indirectSize: IWebresourcesSize;
    uniqueSize: IWebresourcesSize;
}

interface IAsyncChunkDictionary extends IBaseChunkDictionary {
    inheritedSize: IWebresourcesSize;
}

export interface IEntryChunkDictionary extends IBaseChunkDictionary {
    async: Array<IAsyncChunkDictionary>;
}

export const generateExternalSize = (
    entrypointDescriptors: Array<IEntrypointDescriptor>,
    entrypointDependenciesDescriptors: EntryDependencyDescriptor,
    sizeDictionary: ISizeDictionary
    ): Array<IEntryChunkDictionary> => {
    return entrypointDescriptors.map((entrypointDescriptor: IEntrypointDescriptor): IEntryChunkDictionary => {
        const entryDependencyDescriptor = getDependencyDescriptorForChunkName(
            entrypointDependenciesDescriptors,
            entrypointDescriptor.name
        ) as IEntryResourceDependencies;
        return {
            name: entrypointDescriptor.name,
            externalSize: externalSizeForChunkGroup(entryDependencyDescriptor, sizeDictionary),
            directSize: directDependenciesSize(entryDependencyDescriptor, sizeDictionary),
            indirectSize: indirectDependenciesSize(entryDependencyDescriptor, sizeDictionary),
            uniqueSize: uniqueDependenciesSize(entryDependencyDescriptor, sizeDictionary),
            async: entrypointDescriptor.asyncChunks.map((asyncChunk: IChunkGroupDescriptor): IAsyncChunkDictionary => {
                const asyncChunkDependencyDescriptor = getDependencyDescriptorForChunkName(
                    entryDependencyDescriptor.async,
                    asyncChunk.name
                );
                return {
                    name: asyncChunk.name,
                    externalSize: externalSizeForChunkGroup(asyncChunkDependencyDescriptor, sizeDictionary),
                    inheritedSize: inheritedSizeFromEntrypoint(
                        entryDependencyDescriptor,
                        asyncChunkDependencyDescriptor,
                        sizeDictionary
                    ),
                    directSize: directDependenciesSize(asyncChunkDependencyDescriptor, sizeDictionary),
                    indirectSize: indirectDependenciesSize(asyncChunkDependencyDescriptor, sizeDictionary),
                    uniqueSize: uniqueDependenciesSize(asyncChunkDependencyDescriptor, sizeDictionary),
                };
            }),
        };
    });
};
